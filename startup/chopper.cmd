require chopper, 0.1.0
require mrfioc2,2.7.13-ESS0
require pev,0.1.2

#dbLoadRecords("chopper.db")

############################################################
################### Configuration Timing ###################
############################################################
epicsEnvSet("SYS"             "ISrc-010")
epicsEnvSet("EVENT_14HZ"      "14")

################### Configuration EVG ###################
epicsEnvSet("EVG"             "EVG")
epicsEnvSet("EVG_VMESLOT"     "2")

mrmEvgSetupVME($(EVG), $(EVG_VMESLOT), 0x100000, 1, 0x01)

dbLoadRecords("evg-vme-230.db", "DEVICE=$(EVG), SYS=$(SYS), EvtClk-FracSynFreq-SP=88.0525, TrigEvt0-EvtCode-SP=$(EVENT_14HZ), Mxc1-Frequency-SP=14, Mxc1-TrigSrc0-SP=1")

mrmEvgSoftTime("$(EVG)")

################### Configuration EVR ###################
epicsEnvSet("EVR"             "EVR0")
epicsEnvSet("EVR_VMESLOT"     "5")

mrmEvrSetupVME($(EVR), $(EVR_VMESLOT), 0x3000000, 5, 0x026)

dbLoadRecords("evr-vme-230.db", "DEVICE=$(EVR), SYS=$(SYS), Link-Clk-SP=88.0525, FrontOut0-Src-SP=0, FrontOut0-Ena-SP=1, FrontUnivOut0-Src-SP=0, FrontUnivOut0-Ena-SP=1, Pul0-Prescaler-SP=77, Pul0-Width-SP=20000, Pul0-Delay-SP=0, Pul1-Prescaler-SP=77, FrontOut1-Src-SP=1, FrontOut1-Ena-SP=1, Pul1-Prescaler-SP=77, Pul1-Width-SP=10000, Pul1-Delay-SP=0")

dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYS), EVT=$(EVENT_14HZ), PID=0, F=Trig, ID=0")
dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYS), EVT=$(EVENT_14HZ), PID=1, F=Trig, ID=1")
